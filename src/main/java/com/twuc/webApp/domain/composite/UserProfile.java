package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class UserProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false, length = 128)
    private String addressCity;
    @Column(nullable = false, length = 128)
    private String addressStreet;

    public UserProfile() {
    }

    public UserProfile(String addressCity, String addressStreet) {
        this.addressCity = addressCity;
        this.addressStreet = addressStreet;
    }

    public long getId() {
        return id;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public String getAddressStreet() {
        return addressStreet;
    }
}
