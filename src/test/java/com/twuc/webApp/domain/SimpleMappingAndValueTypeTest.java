package com.twuc.webApp.domain;

import com.twuc.webApp.domain.composite.CompanyProfile;
import com.twuc.webApp.domain.composite.CompanyProfileRepository;
import com.twuc.webApp.domain.composite.UserProfile;
import com.twuc.webApp.domain.composite.UserProfileRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SimpleMappingAndValueTypeTest extends JpaTestBase {
    @Autowired
    private UserProfileRepository userProfileRepository;
    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Test
    void should_save_and_get_userProfile_entities() {
        ClosureValue<Long> closureValue = new ClosureValue<>();
        flushAndClear((em) -> {
            UserProfile userProfile = new UserProfile("WH", "JieDaoKou");
            userProfileRepository.save(userProfile);
            closureValue.setValue(userProfile.getId());
        });

        run((em) -> {
            UserProfile userProfile = userProfileRepository.findById(closureValue.getValue()).orElseThrow(RuntimeException::new);
            assertEquals("WH", userProfile.getAddressCity());
            assertEquals("JieDaoKou", userProfile.getAddressStreet());
        });
    }
    @Test
    void should_save_and_get_companyProfile_entities() {
        {
            ClosureValue<Long> closureValue = new ClosureValue<>();
            flushAndClear((em) -> {
                CompanyProfile companyProfile = new CompanyProfile("WHH", "JieDaoKouKou");
                companyProfileRepository.save(companyProfile);
                closureValue.setValue(companyProfile.getId());
            });

            run((em) -> {
                CompanyProfile companyProfile = companyProfileRepository.findById(closureValue.getValue()).orElseThrow(RuntimeException::new);
                assertEquals("WHH", companyProfile.getCity());
                assertEquals("JieDaoKouKou", companyProfile.getStreet());
            });
        }
    }
}
